Running examples
----------------

A local MongoDB running on the default port will be needed as well as the
default `test` database (stuff will be written to it). It must contain a
`migrations` collection.

Alternatively, it's possible to change `example/config.json`.

To test the custom maintenance script:

  % ./bin/mig -p example -rs test

To check what is needed to be run as well as current schema version:

  % ./bin/mig -p example

To run the migrations:

  % ./bin/mig -p example -r
